package com.lkakulia.lecture_14

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class UsersActivity : AppCompatActivity() {

    private val users = mutableListOf<UserModel>()
    private lateinit var adapter: RecyclerViewAdapter
    private var userToChange = 0

    companion object {
        val REQUEST_CODE_ADD = 99
        val REQUEST_CODE_CHANGE = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        //add user button listener
        addUserButton.setOnClickListener({
            val intent = Intent(this, UserActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE_ADD)

        })

        adapter = RecyclerViewAdapter(users, object: ModifyUser {
            override fun onClickRemove(position: Int) {
                users.removeAt(position)
                adapter.notifyItemRemoved(position)
            }

            override fun onClickChange(position: Int) {
                userToChange = position
                val intent = Intent(this@UsersActivity, UserActivity::class.java)
                intent.putExtra("userInfo", users[position])
                startActivityForResult(intent, REQUEST_CODE_CHANGE)
            }
        })

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val user = data?.getParcelableExtra<UserModel>("userInfo")

        if (user != null) {
            if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_ADD) {
                    users.add(0, user)
                    adapter.notifyItemInserted(0)
                    recyclerView.scrollToPosition(0)

            }
            else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CHANGE) {
                users[userToChange].firstname = user.firstname
                users[userToChange].lastname = user.lastname
                users[userToChange].email = user.email

                adapter.notifyItemChanged(userToChange)
            }
        }
    }
}
