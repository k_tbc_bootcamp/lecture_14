package com.lkakulia.lecture_14

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_adapter.view.*

class RecyclerViewAdapter(
    private val items: MutableList<UserModel>,
    private val modifyUser: ModifyUser) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.layout_adapter,
            parent,
            false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private lateinit var model: UserModel

        fun onBind() {
            model = items[adapterPosition]
            itemView.firstnameTextView.text = model.firstname
            itemView.lastnameTextView.text = model.lastname
            itemView.emailTextView.text = model.email

            itemView.removeUserButton.setOnClickListener({
                modifyUser.onClickRemove(adapterPosition)
            })

            itemView.changeUserButton.setOnClickListener({
                modifyUser.onClickChange(adapterPosition)
            })

        }

    }
}