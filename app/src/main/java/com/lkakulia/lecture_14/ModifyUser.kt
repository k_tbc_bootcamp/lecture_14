package com.lkakulia.lecture_14

import android.content.Context

interface ModifyUser {
    fun onClickRemove(position: Int)
    fun onClickChange(position: Int)
}