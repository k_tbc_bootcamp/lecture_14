package com.lkakulia.lecture_14

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        init()
    }

    private fun init() {

        val user = intent.getParcelableExtra<UserModel>("userInfo")

        if (user != null) {
            firstnameEditText.setText(user.firstname)
            lastnameEditText.setText(user.lastname)
            emailEditText.setText(user.email)
        }

        saveUserButton.setOnClickListener({
            val firstname = firstnameEditText.text.toString()
            val lastname = lastnameEditText.text.toString()
            val email = emailEditText.text.toString()

            if (firstname == "" || lastname == "" || email == "") {
                Toast.makeText(this, "Please fill in all the fields", Toast.LENGTH_LONG).show()
            }

            else {
                val user = UserModel(firstname, lastname, email)
                val intent = Intent(this, UsersActivity::class.java)
                intent.putExtra("userInfo", user)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })
    }
}
